<?
define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Context;
use Bitrix\Main\Loader;


if (!empty($_FILES)) {
    $tempFile = $_FILES['file']['tmp_name'];

    $info = pathinfo($_FILES['file']['name']);
    $ext = $info['extension']; // get the extension of the file

    $newname = md5($_FILES['file']['name'].session_id()).".".$ext;

    $target = $_SERVER["DOCUMENT_ROOT"].'/upload/feedback-form-files/'.$newname;

    // Validate the file type
    $fileTypes = array('jpg','jpeg','gif','png', 'zip', 'rar', 'xls', 'pdf', 'doc', 'docx', 'xlsx'); // File extensions

    if (in_array($ext,$fileTypes)) {
        if(move_uploaded_file($tempFile,$target)){
            $success = 1;
        }
        else {
            return 'Invalid files';
        }
    }
    else {
        return 'Invalid files';
    }

    echo $success;
}