<?
define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Context;
use Bitrix\Main\Loader;

$request = Context::getCurrent()->getRequest();

if ($request->isPost()&&$request->isAjaxRequest())
{
    $answer = array();
    $answer['error'] = false;


    $files = explode("|", $_REQUEST["uploaded_files"]);
    foreach($files as $file){
        if($file=="") continue;

        $info = pathinfo($file);
        $ext = $info['extension']; // get the extension of the file
        $newname = md5($file.session_id()).".".$ext;
        $file_path_list[] = '/upload/feedback-form-files/'.$newname;
        $i++;
    }

    foreach ($file_path_list as $file_path) {
        $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$file_path);
        $fid = CFile::SaveFile($arFile, "feedback-save");
        $file_list_id[] = $fid;
    }

    $CONTENT = 'Имя: '.htmlspecialchars($_REQUEST['name']).'<br>';

    if($_REQUEST['phone']){
        $CONTENT .= 'Телефон: '.htmlspecialchars($_REQUEST['phone']).'<br>';
    }
    if($_REQUEST['email']){
        $CONTENT .= 'Email: '.htmlspecialchars($_REQUEST['email']).'<br>';
    }
    if($_REQUEST['wish']){
        $CONTENT .= 'Желаемая должность: '.htmlspecialchars($_REQUEST['wish']).'<br>';
    }
    if($_REQUEST['company']){
        $CONTENT .= 'Компания: '.htmlspecialchars($_REQUEST['company']).'<br>';
    }
    if($_REQUEST['message']){
        $CONTENT .= 'Сообщение: '.htmlspecialchars($_REQUEST['message']).'<br>';
    }
    if($_REQUEST['from']){
        $CONTENT .= 'Откуда: '.htmlspecialchars($_REQUEST['from']).'<br>';
    }
    if($_REQUEST['where']){
        $CONTENT .= 'Куда: '.htmlspecialchars($_REQUEST['where']).'<br>';
    }

    if($_REQUEST['form_type']=='cooperation'){
        $arEventFields['FORM_TYPE'] = 'Сотрудничество';
        $iblock_id = COOPERATION_FORM_IBLOCK_ID;
    }
    if($_REQUEST['form_type']=='vacancy'){
        $arEventFields['FORM_TYPE'] = 'Вакансии';
        $iblock_id = VACANCY_FORM_IBLOCK_ID;
    }
    if($_REQUEST['form_type']=='feedback'){
        $arEventFields['FORM_TYPE'] = 'Обратная связь';
        $iblock_id = FEEDBACK_FORM_IBLOCK_ID;
    }
    if($_REQUEST['form_type']=='delivery'){
        $arEventFields['FORM_TYPE'] = 'Узнать стоимость доставки';
        $iblock_id = DELIVERY_FORM_IBLOCK_ID;
    }

    $arEventFields['MESSAGE_CONTENT'] = $CONTENT;

    AddMessage2Log($_REQUEST,'request');

    CModule::IncludeModule("iblock");
    $el = new CIBlockElement;
    $PROP = array();

    $PROP['PHONE'] = $_REQUEST['phone'];
    $PROP['EMAIL'] = $_REQUEST['email'];
    $PROP['WISH'] = $_REQUEST['wish'];
    $PROP['COMPANY'] = $_REQUEST['company'];
    $PROP['FROM'] = $_REQUEST['from'];
    $PROP['TO'] = $_REQUEST['where'];

    $PROP['FILE'] = $file_list_id;

    $arLoadProductArray = Array(
        "PROPERTY_VALUES"=> $PROP,
        "IBLOCK_ID"      => $iblock_id,
        "NAME"           => htmlspecialchars($_REQUEST['name']),
        "ACTIVE"         => "Y",            // активен
        'PREVIEW_TEXT' => htmlspecialchars($_REQUEST['message']),
    );

    if(!$PRODUCT_ID = $el->Add($arLoadProductArray)){
        $answer['error'] = true;
        $answer['error_text'] = $el->LAST_ERROR;
    }


    if(!CEvent::Send("CALLBACK", SITE_ID, $arEventFields,'N', '', $file_list_id)){
        $answer['error'] = true;
    }

    echo json_encode($answer);
}
