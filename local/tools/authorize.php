<?php
define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Context;
use Bitrix\Main\Loader;

$request = Context::getCurrent()->getRequest();

if ($request->isPost()&&$request->isAjaxRequest())
{
    $answer = array();
    $answer['error'] = false;

    global $USER;
    if (!is_object($USER)) $USER = new CUser;
    $arAuthResult = $USER->Login($_REQUEST['login'], $_REQUEST['password'], "Y");
    $APPLICATION->arAuthResult = $arAuthResult;
    if($arAuthResult['TYPE']=='ERROR'){
        $answer['error'] = true;
        $answer['error_message'] = strip_tags($arAuthResult['MESSAGE']);
    }

    echo json_encode($answer);
}