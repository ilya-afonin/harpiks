<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if($file_id = intval($_REQUEST['file_id'])){
    $file = CFile::GetFileArray($file_id);

    if (file_exists($_SERVER["DOCUMENT_ROOT"].$file['SRC'])) {
        //header('Content-type: '.$file['CONTENT_TYPE']);
        header("Content-Disposition: attachment; filename='".$file['FILE_NAME']."'");
        readfile($_SERVER["DOCUMENT_ROOT"].$file['SRC']);
    }
}