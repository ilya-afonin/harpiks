<?php
define("STOP_STATISTICS", true);
define('NO_AGENT_CHECK', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Context;
use Bitrix\Main\Loader;

$request = Context::getCurrent()->getRequest();

if ($request->isPost()&&$request->isAjaxRequest())
{
    $answer = array();
    $answer['error'] = false;

    global $USER;
    $arResult = $USER->SendPassword('', $_REQUEST['recovery_email']);
    if($arResult["TYPE"] != "OK") {
        //echo "Контрольная строка для смены пароля выслана.";
        $answer['error'] = true;
        $answer['error_message'] = "Введенный email не найден.";
    }

    echo json_encode($answer);
}