<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

$assets = \Bitrix\Main\Page\Asset::getInstance();
Loc::loadMessages(__FILE__);
?>

</div>
<footer class="footer">
  <div class="container">
    <div class="footer__inner">
      <div class="footer__logo">
        <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/footer_logo.php", Array(), Array("MODE" => "html"))?>
      </div>
      <div class="footer__content">
        <div class="footer__row">
          <div class="footer__col">
            <div class="footer__txt">
              <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/footer_address.php", Array(), Array("MODE" => "text"))?>
            </div>
          </div>
          <div class="footer__col">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/footer_links.php", Array(), Array("MODE" => "text"))?>
          </div>
          <div class="footer__col">
            <div class="footer__txt">
              <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/footer_confident.php", Array(), Array("MODE" => "php"))?>
            </div>
          </div>
        </div>
        <div class="footer__row">
          <div class="footer__col">
            <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/footer_creator.php", Array(), Array("MODE" => "php"))?>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<? $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/popups.php", Array(), Array("MODE" => "html")); ?>


</div>

<? if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false):
  $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/yandex_metrika.php", Array(), Array("MODE" => "html"));
endif; ?>

<?
$assets->addJs('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
$assets->addJs(SITE_TEMPLATE_PATH . '/tpl/assets/js/scripts.min.js');
?>

</body>
</html>