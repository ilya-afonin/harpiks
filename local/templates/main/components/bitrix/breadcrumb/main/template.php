<?php

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult)) return "";

$strReturn = '';

$strReturn .= '<div class="bread">';
$strReturn .= ' <ul class="bread__list" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);

    if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
    {
        $strReturn .= '<li class="bread__l-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
        $strReturn .= ' <a class="bread__link" itemprop="item" href="'.$arResult[$index]["LINK"].'"><span itemprop="name">'.$title.'</span></a>';
        $strReturn .= ' <meta itemprop="position" content="'.($index+1).'" />';
        $strReturn .= '</li>';
    }
    else
    {
        $strReturn .= '<li class="bread__l-item">'.$title.'</li>';
    }
}
$strReturn .= ' </ul>';
$strReturn .= '</div>';

return htmlspecialchars_decode($strReturn);