<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<div class="header__nav">
<?php if (!empty($arResult)){ ?>
    <div class="header__nav-section">
        <?$i=0;?>
        <?php foreach($arResult as $arItem){ ?>
            <?if($i == 2):?>
            </div><div class="header__nav-section header__nav-section--tablet">
            <?endif;?>

            <div class="header__nav-item">

                <a class="link<?if($i < 2) echo ' link--bold';?><?if(!empty($arItem["CHILD"])) echo ' link--drop js-drop';?>" href="<?=(!empty($arItem["CHILD"]))?'#':$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>

                <?php if (!empty($arItem["CHILD"])){ ?>
                <div class="header__drop">
                    <?php foreach($arItem["CHILD"] as $child){ ?>
                    <a class="header__dorp-link" href="<?=$child["LINK"]?>"><?=$child["TEXT"]?></a>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
          <?$i++;?>
        <?php } ?>
    </div>
<?php } ?>
</div>