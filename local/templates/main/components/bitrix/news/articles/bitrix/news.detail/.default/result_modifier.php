<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arFilter = Array(
    "IBLOCK_ID" => ARTICLES_IBLOCK_ID,
    "ACTIVE_DATE" => "Y",
    "ACTIVE" => "Y"
);

$res = CIBlockElement::GetList(Array("ACTIVE_FROM" => "DESC"), $arFilter, false, false, Array('ID', 'NAME', 'DETAIL_PAGE_URL'), false);

while ($ob = $res->GetNext()) {
  $objs[] = array(
      "ID" => $ob["ID"],
      "DETAIL_PAGE_URL" => $ob["DETAIL_PAGE_URL"],
      "NAME" => $ob["NAME"],
  );
}

for ($i = 0; $i < count($objs); $i++) {
  if ($objs[$i]["ID"] == $arResult["ID"]) {
    $next = $i + 1;

    if ($next == count($objs)) {
      $next = 0;
    }
  }
}

$arResult["NEXT"]['URL'] = $objs[$next]["DETAIL_PAGE_URL"];
$arResult["NEXT"]['NAME'] = $objs[$next]["NAME"];

FirePHP::getInstance()->info($arResult);