<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if(count($arResult["ITEMS"])>0):?>
  <div class="c-list">
    <div class="c-list__row">

      <?foreach($arResult["ITEMS"] as $k => $arItem):?>

        <?

        $class = '';

        if(in_array($k, array(0,5,10,14,15)))
          $class = 'pic';
        elseif (in_array($k, array(4,11)))
          $class = 'banner';

       ?>
        <?if($class == 'pic'):?>
        <div class="c-list__box c-list__box--pic">
          <a class="c-list__b-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
          <img class="c-list__pic" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem["NAME"]?>">
          <div class="c-list__b-txt">
            <a class="c-list__btn" href="<?=$arItem['DETAIL_PAGE_URL']?>"></a>
            <div class="c-list__wrap-title">
              <div class="c-list__date"><?=$arItem['DATE_CREATE']?></div>
              <div class="c-list__title"><?=$arItem["NAME"]?></div>
            </div>
          </div>
        </div>
        <?elseif ($class == 'banner'):?>
          <div class="c-list__box c-list__box--banner">
            <div class="c-list__pic" style="background-image:url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>)"></div>
            <a class="c-list__b-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
            <a class="c-list__btn" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
            <div class="c-list__b-txt">
              <div class="c-list__wrap-title">
                <div class="c-list__date"><?=$arItem['DATE_CREATE']?></div>
                <div class="c-list__title"><?=$arItem["NAME"]?></div>
              </div>
            </div>
          </div>
        <?else:?>
          <div class="c-list__box">
            <a class="c-list__b-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
            <a class="c-list__btn" href="<?=$arItem["DETAIL_PAGE_URL"]?>"></a>
            <div class="c-list__b-txt">
              <div class="c-list__wrap-title">
                <div class="c-list__date"><?=$arItem['DATE_CREATE']?></div>
                <div class="c-list__title"><?=$arItem["NAME"]?></div>
              </div>
              <div class="c-list__desc"><?=$arItem["PREVIEW_TEXT"]?></div>
            </div>
          </div>
        <?endif?>

      <?endforeach;?>

    </div>
  </div>


  <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
    <?=$arResult["NAV_STRING"]?>
  <?endif;?>

<?endif?>