<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $DB;

/*--------Режем картинки элементов--------*/
if (count($arResult['ITEMS']) > 0){
  foreach ($arResult['ITEMS'] as $i => $arItem){

    $s = CFile::ResizeImageGet(
        $arItem['PREVIEW_PICTURE']['ID'],
        array("width" => 915, "height" => 417),
        BX_RESIZE_IMAGE_EXACT,
        true,
        false
    );

    $arResult['ITEMS'][$i]['PREVIEW_PICTURE']['SRC'] = $s['src'];

    $arResult['ITEMS'][$i]['DATE_CREATE'] = $DB->FormatDate(($arItem['ACTIVE_FROM'])?$arItem['ACTIVE_FROM']:$arItem['TIMESTAMP_X'], "DD.MM.YYYY HH:MI:SS", "DD.MM.YYYY");

  }
}