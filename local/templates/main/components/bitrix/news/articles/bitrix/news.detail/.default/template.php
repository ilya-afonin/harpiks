<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>

          <h1 class="h1"><?=$arResult['NAME']?></h1>
          <div class="c-top__wrap-date">
            <div class="c-top__date"><?=$DB->FormatDate(($arResult['ACTIVE_FROM'])?$arResult['ACTIVE_FROM']:$arResult['TIMESTAMP_X'], "DD.MM.YYYY HH:MI:SS", "DD.MM.YYYY")?></div>
          </div>
        </div>
        <div class="c-top__c-col">
          <a class="link link--bordered link--blue c-top__link" href="<?=$arResult['LIST_PAGE_URL']?>">К списку статей</a>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="i-projects__row">
    <!-- Соотношение сторон 16:8-->
    <div class="c-middle__photo" style="background-image:url(<?=$arResult['DETAIL_PICTURE']['SRC']?>)"></div>
  </div>
  <div class="c-middle">
    <div class="c-middle__content">

      <?=$arResult['DETAIL_TEXT'];?>

      <div class="color-block color-block--aque c-middle__block c-middle__block--linked">
        <a class="color-block__link" href="<?=$arResult['NEXT']['URL']?>"></a>
        <div class="color-block__inner">
          <div class="color-block__next"><?=Loc::getMessage('NEXT')?></div>
          <div class="color-block__title"><?=$arResult['NEXT']['NAME']?></div>
          <a class="color-block__out" href="<?=$arResult['NEXT']['URL']?>"><?=Loc::getMessage('NEXT_GO')?></a>
        </div>
      </div>
    </div>
  </div>
</div>