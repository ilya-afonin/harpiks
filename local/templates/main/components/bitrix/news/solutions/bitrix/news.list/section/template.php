<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>



<?$arSection = $arResult['SECTION']['PATH'][0]?>
<?//FirePHP::getInstance()->info($arSection);?>


<?if(count($arResult["ITEMS"])>0):?>
  <div class="news-list">
    <b><?=$arResult["NAME"]?></b>
    <ul>
      <?foreach($arResult["ITEMS"] as $arItem):?>
        <li><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></li>
      <?endforeach;?>
    </ul>
  </div>


<?endif?>


<div class="welcome">
  <div class="welcome__inner">
    <div class="welcome__aside">
      <div class="welcome__aside-content">
        <div class="welcome__aside-bread">
          <?$APPLICATION->IncludeComponent(
              "bitrix:breadcrumb",
              "main",
              Array(
                  "PATH" => "",
                  "SITE_ID" => "s1",
                  "START_FROM" => "0"
              )
          );?>
        </div>
        <div class="welcome__aside-title h1"><?=$arSection['NAME']?></div>
        <div class="welcome__aside-text"><?=$arSection['DESCRIPTION']?></div>
      </div>
    </div>
    <div class="welcome__media">
      <div class="welcome__media-image" style="background-image:url(<?=CFile::getPath($arSection['PICTURE'])?>)"></div>
    </div>
  </div>
  <div class="welcome__image"><img class="welcome__image-file" src="<?=SITE_TEMPLATE_PATH?>/tpl/assets/images/static/fingerprint_2.svg" alt=""></div>
</div>
<div class="container">
  <div class="components">
    <div class="components__s-name">
      <div class="s-name s-name--left s-name--border">
        <div class="s-name__text">
          <div class="s-name__title"><?=htmlspecialchars_decode($arSection['UF_COMPONENTS'])?></div>
        </div>
      </div>
    </div>
    <div class="components__inner">
      <div class="component">
        <div class="component__step">
          <div class="component__step-num">1</div>
          <div class="component__step-info">
            <div class="component__step-type">Этап</div>
            <div class="component__step-title">Грунтовка<br>основания</div>
          </div>
        </div>
        <div class="component__c-card">
          <div class="c-card"><a class="c-card__link" href="#"></a>
            <div class="c-card__row">
              <div class="c-card__image">
                <!-- 145x205--><img class="c-card__image-file" src="/local/templates/main/tpl/assets/images/content/c-card-img.png" alt="">
              </div>
              <div class="c-card__content">
                <div class="c-card__content-top">
                  <div class="c-card__title">Эпоксидная грунтовка Harpiks</div>
                  <div class="c-card__prices">
                    <div class="c-card__prices-full">4 570 руб.</div>
                    <div class="c-card__prices-detail">500 руб./м<sup>2</suo></div>
                  </div>
                </div>
                <div class="c-card__text">Низковязкий цветной эпоксидный состав  для устройства кварценаполняемых самовыравнивающихся полов на объектах промышленного назначения.</div>
              </div>
            </div>
            <div class="c-list__btn"></div>
          </div>
        </div>
      </div>
      <div class="component">
        <div class="component__step">
          <div class="component__step-num">2</div>
          <div class="component__step-info">
            <div class="component__step-type">Этап</div>
            <div class="component__step-title">Шпатлевка</div>
          </div>
        </div>
        <div class="component__c-card">
          <div class="c-card"><a class="c-card__link" href="#"></a>
            <div class="c-card__row">
              <div class="c-card__image">
                <!-- 145x205--><img class="c-card__image-file" src="/local/templates/main/tpl/assets/images/content/c-card-img.png" alt="">
              </div>
              <div class="c-card__content">
                <div class="c-card__content-top">
                  <div class="c-card__title">Эпоксидная шпатлевка Harpiks</div>
                  <div class="c-card__prices">
                    <div class="c-card__prices-full">4 570 руб.</div>
                    <div class="c-card__prices-detail">500 руб./м<sup>2</suo></div>
                  </div>
                </div>
                <div class="c-card__text">Низковязкий цветной эпоксидный состав  для устройства кварценаполняемых самовыравнивающихся полов на объектах промышленного назначения.</div>
              </div>
            </div>
            <div class="c-list__btn"></div>
          </div>
        </div>
      </div>
      <div class="component">
        <div class="component__step">
          <div class="component__step-num">3</div>
          <div class="component__step-info">
            <div class="component__step-type">Этап</div>
            <div class="component__step-title">Финишный слой</div>
          </div>
        </div>
        <div class="component__c-card">
          <div class="c-card"><a class="c-card__link" href="#"></a>
            <div class="c-card__row">
              <div class="c-card__image">
                <!-- 145x205--><img class="c-card__image-file" src="/local/templates/main/tpl/assets/images/content/c-card-img.png" alt="">
              </div>
              <div class="c-card__content">
                <div class="c-card__content-top">
                  <div class="c-card__title">Эпоксидный пол Harpiks</div>
                  <div class="c-card__prices">
                    <div class="c-card__prices-full">4 570 руб.</div>
                    <div class="c-card__prices-detail">500 руб./м<sup>2</suo></div>
                  </div>
                </div>
                <div class="c-card__text">Низковязкий цветной эпоксидный состав  для устройства кварценаполняемых самовыравнивающихся полов на объектах промышленного назначения.</div>
              </div>
            </div>
            <div class="c-list__btn"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="bq">
    <div class="bq__title">Дополнительные<br>возможности</div>
    <div class="bq__text">Эпоксидные полы могут выглядеть практически как угодно. Возможность колеровать пол в любой цвет по таблице RAL  и добавление акриловых флоков дают сотни комбинаций цветов и фактур.</div>
  </div>
  <div class="offers">
    <div class="offers__card">
      <div class="c-card c-card--min"><a class="c-card__link" href="#"></a>
        <div class="c-card__row">
          <div class="c-card__image">
            <!-- 145x205--><img class="c-card__image-file" src="/local/templates/main/tpl/assets/images/content/c-card-img.png" alt="">
          </div>
          <div class="c-card__content">
            <div class="c-card__content-top">
              <div class="c-card__title">Акриловые флоки Harpiks</div>
              <div class="c-card__prices">
                <div class="c-card__prices-full">4 570 руб.</div>
                <div class="c-card__prices-detail">500 руб./м<sup>2</sup></div>
              </div>
            </div>
            <div class="c-card__text">Низковязкий цветной эпоксидный состав  для устройства кварценаполняемых самовыравнивающихся полов на объектах промышленного назначения.</div>
          </div>
        </div>
        <div class="c-list__btn"></div>
      </div>
    </div>
    <div class="offers__card">
      <div class="c-card c-card--min"><a class="c-card__link" href="#"></a>
        <div class="c-card__row">
          <div class="c-card__image">
            <!-- 145x205--><img class="c-card__image-file" src="/local/templates/main/tpl/assets/images/content/c-card-img.png" alt="">
          </div>
          <div class="c-card__content">
            <div class="c-card__content-top">
              <div class="c-card__title">Защитный лак Harpiks</div>
              <div class="c-card__prices">
                <div class="c-card__prices-full">4 570 руб.</div>
                <div class="c-card__prices-detail">500 руб./м<sup>2</suo></div>
              </div>
            </div>
            <div class="c-card__text">Низковязкий цветной эпоксидный состав  для устройства кварценаполняемых самовыравнивающихся полов на объектах промышленного назначения.</div>
          </div>
        </div>
        <div class="c-list__btn"></div>
      </div>
    </div>
  </div>
  <div class="types">
    <div class="types__item types__item--brown"><a class="types__item-link" href="#"></a>
      <div class="types__item-name">Полиуретановые<br>полы</div>
    </div>
    <div class="types__item types__item--blue"><a class="types__item-link" href="#"></a>
      <div class="types__item-name">Спортивные<br>полы</div>
    </div>
    <div class="types__item types__item--light"><a class="types__item-link" href="#"></a>
      <div class="types__item-name">Краски и лаки</div>
    </div>
  </div>
  <div class="c-article"><a class="c-article__link" href="#"></a>
    <div class="c-article__image" style="background-image:url(assets/images/content/c-article.jpg)"></div>
    <div class="c-article__content">
      <div class="c-article__date">24.07.18</div>
      <div class="c-article__title h2">Как правильно подобрать материал</div>
      <div class="c-article__text">В зависимости от количества комплектов, груз может быть поставлен на паллеты или ехать отдельно с обрешёткой. Основа – 12 месяцев, отвердитель - 6 месяцев со дня изготовления. Хранить в таре производителя; в помещении...</div>
      <div class="c-list__btn"></div>
    </div>
  </div>
</div>


<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
  <br /><?=$arResult["NAV_STRING"]?>
<?endif;?>