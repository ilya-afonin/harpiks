<?
$rsResult = CIBlockSection::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arResult['SECTION']['PATH'][0]['ID']), false, $arSelect = array("UF_*")); // возвращаем список разделов с нужными нам пользовательскими полями. UF_* - в таком виде выведет все доступные для данного раздела поля.

if($arSection = $rsResult -> GetNext())
{
  $arResult['SECTION']['PATH'][0] = array_merge($arResult['SECTION']['PATH'][0], $arSection);
}