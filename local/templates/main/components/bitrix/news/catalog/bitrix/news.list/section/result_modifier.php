<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$cp = $this->__component; // объект компонента


/*--------Вынимаем информацию по текущей секции и остальным на этом же уровне--------*/
$rsResult = CIBlockSection::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "DEPTH_LEVEL" => $arResult['SECTION']['PATH'][0]['DEPTH_LEVEL']), false, $arSelect = array("ID", "DEPTH_LEVEL", "DETAIL_PAGE_URL", "SECTION_PAGE_URL", "NAME", "PREVIEW_TEXT", "PREVIEW_PICTURE", "UF_*"));

while($arSec = $rsResult->GetNext())
{

  if($arSec['ID'] == $arResult['SECTION']['PATH'][0]['ID']){
    $arResult['SECTION'] = array_merge($arResult['SECTION']['PATH'][0], $arSec);
  }

  else {

    $arResult['OTHER_SECTIONS'][] = $arSec;

  }

}

/*--------Картинка для секции--------*/
$secPic = CFile::ResizeImageGet(
    $arResult['SECTION']['PICTURE'],
    array("width" => 1385, "height" => 900),
    BX_RESIZE_IMAGE_EXACT,
    true,
    false
);

$arResult['SECTION']['PICTURE'] = $secPic['src'];


/*--------Режем картинки элементов--------*/
if (count($arResult['ITEMS']) > 0){
  foreach ($arResult['ITEMS'] as $i => $arItem){

    $s = CFile::ResizeImageGet(
        $arItem['PREVIEW_PICTURE']['ID'],
        array("width" => 145, "height" => 205),
        BX_RESIZE_IMAGE_EXACT,
        true,
        false
    );
    $arResult['ITEMS'][$i]['PREVIEW_PICTURE']['SRC'] = $s['src'];


    $d = CFile::ResizeImageGet(
        $arItem['DETAIL_PICTURE']['ID'],
        array("width" => 540, "height" => 550),
        BX_RESIZE_IMAGE_EXACT,
        true,
        false
    );
    $arResult['ITEMS'][$i]['DETAIL_PICTURE']['SRC'] = $d['src'];

  }
}


/*--------Собираем элементы по этапам--------*/
// получаем разделы
$dbResSect = CIBlockSection::GetList(
    Array("SORT"=>"ASC"),
    Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "SECTION_ID" => $arResult['SECTION']['ID'])
);
//Получаем разделы и собираем в массив
while($sectRes = $dbResSect->GetNext())
{
  $arSections[] = $sectRes;
}
//Собираем  массив из Разделов и элементов
foreach($arSections as $arSection){
  foreach($arResult["ITEMS"] as $key=>$arItem){
    if($arItem['IBLOCK_SECTION_ID'] == $arSection['ID'] && $arItem['PROPERTIES']['SHOW_IN_CATALOG']['VALUE'] == 'Да' ){
      $arSection['ELEMENTS'][] =  $arItem;
    }
  }
  $arElementGroups[] = $arSection;
}
$arResult["ITEMS"] = $arElementGroups;



/*--------Собираем дополнительные элементы из каталога--------*/

if(count($arResult['SECTION']['UF_CATALOG_ITEMS']) > 0){

  $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL", "PREVIEW_TEXT", "PREVIEW_PICTURE", "PROPERTY_PRICE", "PROPERTY_VOLUME", "PROPERTY_CATALOG_NAME");
  $arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "ID" => $arResult['SECTION']['UF_CATALOG_ITEMS'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
  $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>2), $arSelect);

  $items = array();

  while($ob = $res->GetNext())
  {

    $itemsPic = CFile::ResizeImageGet(
        $ob['PREVIEW_PICTURE'],
        array("width" => 140, "height" => 205),
        BX_RESIZE_IMAGE_EXACT,
        true,
        false
    );

    $ob['PREVIEW_PICTURE'] = $itemsPic['src'];

    $items[] = $ob;
  }
  $arResult['MORE_ITEMS'] = $items;
}

/*--------Собираем статью--------*/

if(!empty($arResult['SECTION']['UF_ARTICLE'])){

  $arSelect = Array("ID", "NAME", "ACTIVE_FROM", "DATE_CREATE", "DETAIL_PAGE_URL", "PREVIEW_TEXT", "PREVIEW_PICTURE");
  $arFilter = Array("IBLOCK_ID"=> 5, "ID" => $arResult['SECTION']['UF_ARTICLE'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
  $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);

  if($ob = $res->GetNext())
  {
    $arResult['ARTICLE'] = $ob;
    $arResult['ARTICLE']['DATE_CREATE'] = $DB->FormatDate($ob['DATE_CREATE'], "DD.MM.YYYY HH:MI:SS", "DD.MM.YYYY");

    $artPic = CFile::ResizeImageGet(
        $ob['PREVIEW_PICTURE']['ID'],
        array("width" => 675, "height" => 415),
        BX_RESIZE_IMAGE_EXACT,
        true,
        false
    );

    $arResult['ARTICLE']['PREVIEW_PICTURE'] = $artPic['src'];

  }

}

$cp->SetResultCacheKeys(array('SECTION', 'OTHER_SECTIONS', 'MORE_ITEMS', 'ARTICLE'));


