<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>


<?
global $APPLICATION;
$arSection = $arResult['SECTION'] ?>

  <div class="welcome__aside-title h1"><?= $arSection['NAME'] ?></div>
  <div class="welcome__aside-text"><?= $arSection['DESCRIPTION'] ?></div>
  </div>
  </div>
  <div class="welcome__media">
    <div class="welcome__media-image" style="background-image:url(<?=$arSection['PICTURE'];?>)"></div>
  </div>
  </div>
  <div class="welcome__image"><img class="welcome__image-file"
                                   src="<?= SITE_TEMPLATE_PATH ?>/tpl/assets/images/static/fingerprint_2.svg" alt="">
  </div>
  </div>

  <div class="container">
    <div class="components">
      <div class="components__s-name">
        <div class="s-name s-name--left s-name--border">
          <div class="s-name__text">
            <div class="s-name__title"><?= htmlspecialchars_decode($arSection['UF_COMPONENTS']) ?></div>
          </div>
        </div>
      </div>
      <div class="components__inner">

        <? if (count($arResult["ITEMS"]) > 0): ?>


          <? foreach ($arResult["ITEMS"] as $k => $arSection): ?>
            <div class="component">
              <div class="component__step">
                <div class="component__step-num"><?= $k + 1 ?></div>
                <div class="component__step-info">
                  <div class="component__step-type">Этап</div>
                  <div class="component__step-title"><?= $arSection['NAME'] ?></div>
                </div>
              </div>
              <? foreach ($arSection["ELEMENTS"] as $key => $arItem): ?>
                <div class="component__c-card">
                  <div class="c-card">
                    <a class="c-card__link" href="<?=$arItem['DETAIL_PAGE_URL']?>"></a>
                    <div class="c-card__row">
                      <div class="c-card__image">
                       <img class="c-card__image-file" src="<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>" alt="<?= $arItem["NAME"]; ?>">
                      </div>
                      <div class="c-card__content">
                        <div class="c-card__content-top">
                          <div class="c-card__title"><?= $arItem['PROPERTIES']['CATALOG_NAME']['VALUE'] ?></div>
                          <div class="c-card__prices">
                            <div class="c-card__prices-full"><?= $arItem['PROPERTIES']['PRICE']['VALUE'] ?> руб.</div>
                            <div class="c-card__prices-detail"><?= $arItem['PROPERTIES']['VOLUME']['VALUE'] ?> руб./м<sup>2</sup></div>
                          </div>
                        </div>
                        <div class="c-card__text"><?= $arItem['PREVIEW_TEXT'] ?></div>
                      </div>
                    </div>
                    <div class="c-list__btn"></div>
                  </div>
                </div>
              <? endforeach; ?>
            </div>
          <? endforeach; ?>
        <? endif ?>
      </div>
    </div>

    <? $APPLICATION->IncludeComponent(
        "custom:london.smartbanner",
        "banner",
        array(
            "IBLOCK_ID" => "4",
            "IBLOCK_SECTION" => "",
            "IBLOCK_TYPE" => "LONDON_SMART_BANNER",
            "COMPONENT_TEMPLATE" => "banner"
        ),
        false
    ); ?>

    <div class="offers">
      <?foreach ($arResult['MORE_ITEMS'] as $arMoreItem):?>
      <div class="offers__card">
        <div class="c-card c-card--min"><a class="c-card__link" href="<?=$arMoreItem['DETAIL_PAGE_URL']?>"></a>
          <div class="c-card__row">
            <div class="c-card__image">
             <img class="c-card__image-file" src="<?=$arMoreItem['PREVIEW_PICTURE']?>" alt="<?=$arMoreItem['NAME']?>">
            </div>
            <div class="c-card__content">
              <div class="c-card__content-top">
                <div class="c-card__title"><?=$arMoreItem['PROPERTY_CATALOG_NAME_VALUE']?></div>
                <div class="c-card__prices">
                  <div class="c-card__prices-full"><?=$arMoreItem['PROPERTY_PRICE_VALUE']?> руб.</div>
                  <div class="c-card__prices-detail"><?=$arMoreItem['PROPERTY_VOLUME_VALUE']?> руб./м<sup>2</sup></div>
                </div>
              </div>
              <div class="c-card__text"><?=$arMoreItem['PREVIEW_TEXT']?></div>
            </div>
          </div>
          <div class="c-list__btn"></div>
        </div>
      </div>
      <?endforeach;?>
    </div>
    <?if(count($arResult['OTHER_SECTIONS']) > 0):?>
      <div class="types">
        <?foreach ($arResult['OTHER_SECTIONS'] as $i => $arOther):?>
        <div class="types__item types__item--<?if($i==0) echo 'brown'; elseif($i == 1) echo 'blue'; else echo 'light'?>">
          <a class="types__item-link" href="<?=$arOther['SECTION_PAGE_URL']?>"></a>
          <div class="types__item-name"><?=$arOther['NAME']?></div>
        </div>
        <?endforeach;?>
      </div>
    <?endif;?>

    <?if(!empty($arResult['ARTICLE'])):?>
    <div class="c-article">
      <a class="c-article__link" href="<?=$arResult['ARTICLE']['DETAIL_PAGE_URL'];?>"></a>
      <div class="c-article__image" style="background-image:url(<?=$arResult['ARTICLE']['PREVIEW_PICTURE'];?>)"></div>
      <div class="c-article__content">
        <div class="c-article__date"><?=$arResult['ARTICLE']['DATE_CREATE']?></div>
        <div class="c-article__title h2"><?=$arResult['ARTICLE']['NAME']?></div>
        <div class="c-article__text"><?=TruncateText($arResult['ARTICLE']['PREVIEW_TEXT'], 217);?>...</div>
        <div class="c-list__btn"></div>
      </div>
    </div>
    <?endif;?>
  </div>