<a class="header__logo" href="/">
  <div class="header__logo-max"><img class="header__logo-max-image" src="/local/templates/main/tpl/assets/images/static/logo-max.svg" alt="">
    <div class="header__logo-max-text">Надежность, качество, долговечность</div>
  </div>
  <div class="header__logo-min"><img class="header__logo-min-image" src="/local/templates/main/tpl/assets/images/static/logo-min.svg" alt=""></div>
</a>