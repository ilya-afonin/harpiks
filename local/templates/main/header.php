<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
  die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Loader;
Loc::loadMessages(__FILE__);
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID;?>">

<head>
  <title><?$APPLICATION->ShowTitle();?></title>
  <meta charset="<?=LANG_CHARSET;?>">
  <?$APPLICATION->ShowHead();?>

  <?Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/tpl/assets/css/styles.css"); ?>

  <?Asset::getInstance()->addString('<meta http-equiv="X-UA-Compatible" content="IE=edge">');?>
  <?Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1">');?>
  <?Asset::getInstance()->addString('<link rel="shortcut icon" type="image/x-icon" href="'.SITE_TEMPLATE_PATH.'/tpl/assets/images/static/favicons/favicon.ico">');?>
  <?Asset::getInstance()->addString('<link rel="icon" type="image/x-icon" href="'.SITE_TEMPLATE_PATH.'/tpl/assets/images/static/favicons/favicon.ico">');?>
  <?Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="180x180" href="'.SITE_TEMPLATE_PATH.'/tpl/assets/images/static/favicons/apple-touch-icon.png">')?>
  <?Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="192x192"  href="'.SITE_TEMPLATE_PATH.'/tpl/assets/images/static/favicons/android-chrome-192x192.png">')?>
  <?Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="256x256"  href="'.SITE_TEMPLATE_PATH.'/tpl/assets/images/static/favicons/android-chrome-256x256.png">')?>
  <?Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="32x32" href="'.SITE_TEMPLATE_PATH.'/tpl/assets/images/static/favicons/favicon-32x32.png">')?>
  <?Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="16x16" href="'.SITE_TEMPLATE_PATH.'/tpl/assets/images/static/favicons/favicon-16x16.png">')?>

  <?Asset::getInstance()->addString('<link rel="canonical" href="http://'.SITE_SERVER_NAME.'">')?>
  <?Asset::getInstance()->addString('<meta property="og:url" content="http://'.SITE_SERVER_NAME.'" />')?>
  <?Asset::getInstance()->addString('<meta property="og:title" content="HARPIKS | ПОСТАВКА МОНТАЖ ПОЛИМЕРНЫХ ПОЛОВ"/>')?>
  <?Asset::getInstance()->addString('<meta property="og:description" content="Поставка и монтаж полимерных полов. Опытная бригада рабочих. Низкие цены за кв.м." />')?>
  <?Asset::getInstance()->addString('<meta property="og:type" content="website" />')?>
  <?Asset::getInstance()->addString('<meta property="og:image" content="'.SITE_TEMPLATE_PATH.'/tpl/assets/images/content/og_harpiks.png" />')?>
  <?Asset::getInstance()->addString('<meta name="format-detection" content="telephone=no" />')?>

</head>
<body>
<div id="panel">
  <?$APPLICATION->ShowPanel();?>
</div>

<div class="page">
  <header class="header">
    <div class="header__inner">

      <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/header_logo.php", Array(), Array("MODE" => "php"))?>

      <?php
      $APPLICATION->IncludeComponent(
          "bitrix:menu",
          "top",
          array(
              "ROOT_MENU_TYPE" => "top",
              "CHILD_MENU_TYPE" => "left",
              "MENU_CACHE_TYPE" => "A",
              "MENU_CACHE_TIME" => "36000006",
              "MENU_CACHE_USE_GROUPS" => "Y",
              "MENU_CACHE_GET_VARS" => array(
              ),
              "CACHE_SELECTED_ITEMS" => "N",
              "MAX_LEVEL" => "2",
              "USE_EXT" => "Y",
              "DELAY" => "N",
              "ALLOW_MULTI_SELECT" => "N",
              "COMPONENT_TEMPLATE" => "top"
          ),
          false
      );
      ?>

      <div class="header__burger">
        <div class="header__burger-bar"></div>
        <div class="header__burger-bar"></div>
        <div class="header__burger-bar"></div>
      </div>
      <div class="header__phone">
        <?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/include_areas/" . LANGUAGE_ID . "/header_phone.php", Array(), Array("MODE" => "php"))?>
        <div class="header__phone-button" data-popup="callback"></div>
      </div>
    </div>
  </header>
  <div class="content">



