$(document).ready(function () {

	headerDrop();

	validateForm();

	popups();

	mask();

	headerScroll();

	pSlider();

	calculate();

	sSlider();

	headerDrop();

	paddingTop();

	footerSvg();

	welcome();

	wow();

	faq();

	examplesSlider();

	videos();

	fancy();

	parallax();

	headerBurger();

	footerMob();

	cTopMob();

	tableMob();

	customScroll();

	cCardMob();

	mobDDrop();

	mobDSidebar();

	mobGood();

	mobGProduction();

	mobCArticle();

	mobICalc();

});

const headerDrop = () => {

	$('.js-drop').on('touchstart', function () {

		if (!($(this).hasClass('clicked'))) {

			$('.js-drop').removeClass('clicked');

			$(this).addClass('clicked');

			return false;
		}
	})
}

const validateForm = () => {

	if ($('.js-validate').length > 0) {

		$('.js-validate').each(function () {

			$(this).validate({
				rules: {
					name: "required",
					phone: "required",
					city: 'required',
					mail: {
						required: true,
						email: true
					}
				},

				messages: {
					name: "Введите имя",
					mail: "Введите почту",
					phone: "Введите телефон"
				},

				submitHandler: function () {

					$('.popup__form').fadeOut(300, function () {

						$('.popup__success').fadeIn();
					});

					var counter = 5;

					$('.popup__success-timer-value').text(counter);

					var interval = setInterval(function () {

						counter -= 1;

						$('.popup__success-timer-value').text(counter);

						switch (true) {

							case (counter <= 1):
								$('.popup__success-timer-sec').text('секунду');
								break;

							case (counter >= 2 && counter <= 4):
								$('.popup__success-timer-sec').text('секунды');
								break;

							case (counter == 0):
								$('.popup__success-timer-sec').text('секунд');
								break;

							default:
								$('.popup__success-timer-sec').text('секунд');
								break;
						}

						if (counter == 0) {

							$('.popup').removeClass('popup--visible');

							$('body').removeClass('is-unscrolled');

							clearTimeout(interval);
						}
					}, 1000);
				}
			});
		});
	}
}

const popups = () => {

	if ($('.popup').length > 0) {

		$('body').on('click', '[data-popup]', function () {

			var needPopup = $(this).attr('data-popup');

			$('#' + needPopup).addClass('popup--visible');

			$('body').addClass('is-unscrolled');

			return false;
		});

		$('body').on('click', '.popup__closer', function () {

			$('.popup').removeClass('popup--visible');

			$('body').removeClass('is-unscrolled');

		});

		$(document).keyup(function (e) {

			if (e.keyCode === 27) {

				$('.popup').removeClass('popup--visible');

				$('body').removeClass('is-unscrolled');
			}
		});

		$(document).on('click', function (e) {

			var block = $('.popup__container');

			if (!block.is(e.target) &&
				block.has(e.target).length === 0 && $('.popup').hasClass('popup--visible')) {

				$('.popup').removeClass('popup--visible');

				$('body').removeClass('is-unscrolled');
			}
		});
	}
}

const mask = () => {

	if ($('.phone').length > 0) {

		$(".phone").mask('+7(999)-999-99-99');

	}
}

const headerScroll = () => {

	let lastScrollTop = 0;

	$(window).on('scroll', function () {

		let scrolled = $(window).scrollTop();

		if (scrolled > 300) {

			$('.header').addClass('header--scrolled');

			if (scrolled > lastScrollTop) {
				$('.header').removeClass('header--visible');
			} else {
				$('.header').addClass('header--visible');
			}
		} else {

			$('.header').removeClass('header--scrolled');
			$('.header').removeClass('header--visible');
		}

		lastScrollTop = scrolled <= 0 ? 0 : scrolled;
	});
}

const pSlider = () => {

	if ($('.p-slider').length > 0) {

		$('.p-slider').each(function () {

			let slider = $(this);
			let scene = slider.find('.p-slider__scene');

			let navigation = slider.find('.p-slider__controls-inner');

			let current = slider.find('.p-slider__currrent');
			let value = slider.find('.p-slider__value');

			scene.owlCarousel({
				items: 1,
				nav: true,
				navText: '',
				dots: false,
				navContainer: navigation,
				navElement: 'a',
				navClass: ['p-slider__arr p-slider__arr--prev', 'p-slider__arr p-slider__arr--next'],
				onInitialized: init,
				onTranslate: init
			});

			function init(event) {
				current.text(event.item.index + 1);
				value.text(event.item.count);
			}
		});
	}
}

const calculate = () => {

	if ($('.i-calc').length > 0) {

		let field = $('.i-calc__input-field');

		field.on('keyup', function () {

			if (field.val().length > 0) {
				$('.i-calc__output').addClass('is-visible');
			} else {
				$('.i-calc__output').removeClass('is-visible');
			}
		});
	}
}

const sSlider = () => {

	if ($('.s-slider').length > 0) {

		$('.s-slider').each(function () {

			let that = $(this)
			let scene = that.find('.s-slider__scene')
			let navigation = that.find('.s-slider__controls')

			let current = that.find('.p-slider__currrent')
			let value = that.find('.p-slider__value')

			setTimeout(function () {
				scene.owlCarousel({
					// items: 4,
					// slideBy: 4,
					dots: false,
					nav: true,
					navText: '',
					navContainer: navigation,
					navElement: 'a',
					navClass: ['p-slider__arr p-slider__arr--prev', 'p-slider__arr p-slider__arr--next'],
					onInitialized: init,
					onTranslate: init,
					responsive: {
						0: {
							margin: 0,
							items: 1,
							slideBy: 1,
						},
						768: {
							margin: 15,
							items: 4,
							slideBy: 4,
						},
						1280: {
							margin: 50
						},
						1600: {
							margin: 0
						},
					}
				})
			}, 5000)

			function init(event) {
				current.text(((event.item.index / event.page.size) + 1).toFixed(0));
				value.text(event.item.count / event.page.size);
			}
		});
	}
}

const paddingTop = () => {
	let checkEl = document.querySelectorAll('.c-top');
	if (checkEl.length === 0) return false;

	let el = document.querySelector('.c-top'),
		header = document.querySelector('.header');

	el.style.paddingTop = header.offsetHeight + 'px';

	return false;
}

const footerSvg = () => {

	let checkEl = document.querySelectorAll('.footer');
	if (checkEl.length === 0) return false;

}

const welcome = () => {

	if ($('.welcome__media-item').length > 0) {

		let image = $('.welcome__media-image');
		let welcomeFly = $('.welcome__image');

		$(window).on('scroll', function () {
			let scrolled = $(window).scrollTop();
			image.css('bottom', (0 - (scrolled * .35)) + 'px');
		})

		let timer = false
		let block
		let idx = 0

		$('.welcome__feature').on('mouseenter', function () {

			if (block != $(this).index()) {
				block = $(this).index()
				idx = 0
			}
		})

		$('.welcome__feature').on('mousemove', function () {

			let that = $(this)

			if (timer == false && idx != 1) {

				idx += 1;

				timer = true;

				setTimeout(function () {

					timer = false;

				}, 1000);

				let index = that.index();
				let needItem = $('.welcome__media-item').eq(index);

				needItem.addClass('is-active');
				$('.welcome__media-item').not(needItem).removeClass('is-active')
			}
		});
	}
}

const wow = () => {
	new WOW().init();
}

const faq = () => {

	if ($('.faq').length === 0) return false

	$('.faq__ask').on('click', function () {

		$(this).toggleClass('is-active')
		$(this).find('.faq__ask-answer').slideToggle()
	})

	$('.faq__theme').on('click', function () {

		if (!$(this).hasClass('is-active')) {

			let index = $(this).index();

			$('.faq__theme').removeClass('is-active')

			$(this).addClass('is-active');

			$('.faq__asks-item.is-active').fadeOut(200, function () {
				$('.faq__asks-item').eq(index).fadeIn(200)
				$('.faq__asks-item.is-active').removeClass('is-active')
				$('.faq__asks-item').eq(index).addClass('is-active')
			})
		}
	})

	// for mob
	if ($(window).width() >= 768) return false;

	let faq = $('.faq');
	let themes = $('.faq__themes');
	let drop = $('<div class="faq__drop"></div>').prependTo(faq);
	let title = $('<div class="faq__w-title"></div>').prependTo(drop);

	themes.appendTo(drop);

	$('.faq__theme').each(function () {
		if ($(this).hasClass('is-active')) {
			let themeText = $(this).text();
			title.text(themeText);
		}
	});

	$('.faq__theme').on('click', function () {
		if ($(this).hasClass('is-active')) {
			let themeText = $(this).text();
			title.text(themeText);
			themes.css('top', title.innerHeight() + 'px')
		}
	});

	title.on('click', function (e) {
		themes.toggleClass('faq__themes--show');
		$(this).toggleClass('faq__w-title--show');
	});

	$(document).on('click', function (e) {
		let block = $('.faq__drop');

		if (!block.is(e.target) && block.has(e.target).length === 0) {
			themes.removeClass('faq__themes--show');
			title.removeClass('faq__w-title--show');
		}
	});

	// 
	return false;
}


const examplesSlider = () => {

	if ($('.examples').length > 0) {

		var slider1 = tns({
			container: ".examples__scene",
			nested: "inner",
			loop: false,
			slideBy: "page",
			swipeAngle: false,
			speed: 400,
			dots: false,
			controlsContainer: '.examples__arrows',
			responsive: {
				0: {
					gutter: 0,
					items: 1
				},
				320: {
					gutter: 30
				},
				1280: {
					items: 2,
					gutter: 70
				}
			}
		});

		$('.t-slider').each(function () {

			let index = $(this).parents('.item').index()
			let wrapper = $(this).parents('.item')
			let navigation = wrapper.find('.p-slider__controls-inner')

			$(this).addClass('tiny' + index)
			$(this).parents('.item').find('.p-slider__arrows').addClass('p-slider_' + index)

			let slider = tns({
				container: '.tiny' + index,
				items: 1,
				nested: "inner",
				controlsContainer: '.p-slider_' + index,
				mouseDrag: true,
				rewind: false,
				loop: false,
			});

			let info = slider.getInfo();

			wrapper.find('.p-slider__value').text(info.pages);

			wrapper.find('.p-slider__currrent').text('1');

			slider.events.on('indexChanged', (event) => {

				wrapper.find('.p-slider__currrent').text(event.index + 1)
			})
		})
	}
}

const fancy = () => {

	if ($('[data-fancybox="gallery"]').length > 0) {

		$('[data-fancybox="gallery"]').fancybox({

			buttons: [
				"zoom",
				"close"
			]
		});
	}
}

const parallax = () => {

	if ($('#scene_1').length > 0) {
		let scene = $('#scene_1').get(0);
		let parallaxInstance = new Parallax(scene, {
			relativeInput: true
		});
	}
}

const headerBurger = () => {

	$('.header__burger').on('click', function () {

		$(this).toggleClass('is-active')
		$('.header').toggleClass('is-opened')
	})
}

var videos = function videos() {
	if ($('.js-youtube').length > 0) {
		$('.js-youtube').YouTubePopUp();
	}
};

const footerMob = () => {
	let checkEl = document.querySelectorAll('.footer');
	if (checkEl.length === 0) return false;

	let footerRow = document.querySelectorAll('.footer__row'),
		firstRow = footerRow[0],
		secondRow = footerRow[1],
		secondRowChildren = firstRow.querySelectorAll('.footer__col'),
		firstRowChildren = firstRow.querySelectorAll('.footer__col');

	let check = () => {
		let winWidth = document.documentElement.clientWidth || document.body.clientWidth;
		if (winWidth < 1280) {
			secondRow.appendChild(firstRowChildren[firstRowChildren.length - 1]);
		} else if (winWidth >= 1280) {
			firstRow.appendChild(secondRowChildren[secondRowChildren.length - 1]);
		}
	}

	check();
	window.addEventListener('resize', check);
	//
	return false;
}

const cTopMob = () => {
	let checkEl = document.querySelectorAll('.c-top__link');
	if (checkEl.length === 0) return false;

	let link = document.querySelector('.c-top__link'),
		col = document.querySelectorAll('.c-top__c-col'),
		secondCol = col[1],
		wrapDate = document.querySelector('.c-top__wrap-date');

	let check = () => {
		let winWidth = document.documentElement.clientWidth || document.body.clientWidth;

		if (winWidth < 768) {
			wrapDate.appendChild(link);
		} else if (winWidth >= 768) {
			secondCol.appendChild(link);
		}
	}

	check();
	window.addEventListener('resize', check);
	// 
	return false;
}

const tableMob = () => {
	let checkEl = document.querySelectorAll('.c-middle table');
	if (checkEl.length === 0) return false;

	let tables = document.querySelectorAll('.c-middle table');

	let wrap = (el, wrapper) => {
		el.parentNode.insertBefore(wrapper, el);
		wrapper.appendChild(el);
	}

	let check = () => {
		let winWidth = document.documentElement.clientWidth || document.body.clientWidth;
		if (winWidth >= 1280) return false;

		tables.forEach((el, i) => {
			let wrapper = document.createElement('div');
			wrapper.classList.add('c-middle__wrap-table');
			wrap(el, wrapper);
		});
	}

	check();
	window.addEventListener('resize', check);

	// 
	return false;
}

const customScroll = () => {
	if ($('.c-middle__wrap-table').length === 0) return false;

	$('.c-middle__wrap-table').mCustomScrollbar({
		axis: 'x',
		scrollButtons: {
			enable: true
		}
	});

	// 
	return false;
}

const cCardMob = () => {
	let checkEl = document.querySelectorAll('.c-card');
	if (checkEl.length === 0) return false;

	let cards = document.querySelectorAll('.c-card');

	[].forEach.call(cards, (el) => {
		let card = el,
			prices = card.querySelector('.c-card__prices'),
			content = card.querySelector('.c-card__content'),
			contentTop = card.querySelector('.c-card__content-top'),
			title = card.querySelector('.c-card__title'),
			picWrap = card.querySelector('.c-card__image');

		let check = () => {
			let winWidth = document.documentElement.clientWidth || document.body.clientWidth;
			if (winWidth < 1280) {
				content.appendChild(prices);
			} else if (winWidth >= 1280) {
				contentTop.appendChild(prices);
			}

			if (winWidth < 768) {
				picWrap.appendChild(title);
			} else if (winWidth >= 768) {
				content.insertBefore(title, content.firstChild);
			}
			return false;
		}

		check();
		window.addEventListener('resize', check);
	})

	// 
	return false;
}

const mobDDrop = () => {
	let checkEl = document.querySelectorAll('.d-card');
	if (checkEl.length === 0) return false;

	let dСards = document.querySelectorAll('.d-card');

	[].forEach.call(dСards, (el, i) => {
		let drop = el.querySelector('.d-drop'),
			close = drop.querySelector('.d-drop__close');

		el.addEventListener('click', (event) => {
			if (!close.contains(event.target)) {
				drop.classList.add('d-drop--active');
			}
		});

		close.addEventListener('click', (event) => {
			if (close.contains(event.target)) {
				drop.classList.remove('d-drop--active');
			}
		});

		document.addEventListener('click', event => {
			if (!el.contains(event.target)) {
				drop.classList.remove('d-drop--active');
			}
		});
	});

	// 
	return false;
}

const mobDSidebar = () => {
	let checkEl = document.querySelectorAll('.d-sidebar__items');
	if (checkEl.length === 0) return false;

	let section = document.querySelectorAll('.d-sidebar__section');

	[].forEach.call(section, el => {
		let sidebar = el.querySelector('.d-sidebar__items'),
			dTitle = document.createElement('div'),
			wrapper = document.createElement('div'),
			arrTitle = [],
			txtTitle,
			items = sidebar.querySelectorAll('.d-sidebar__item');

		wrapper.classList.add('d-sidebar__d-wrap');
		el.insertBefore(wrapper, el.children[1]);

		dTitle.classList.add('d-sidebar__d-title');
		dTitle.textContent = 'Выберите';
		wrapper.appendChild(dTitle);

		wrapper.appendChild(sidebar);
		sidebar.style.top = dTitle.clientHeight - 3 + 'px';

		dTitle.addEventListener('click', () => {
			sidebar.classList.toggle('d-sidebar__items--show');
			dTitle.classList.toggle('d-sidebar__d-title--show');
		});

		document.addEventListener('click', event => {
			if (!el.contains(event.target)) {
				sidebar.classList.remove('d-sidebar__items--show');
				dTitle.classList.remove('d-sidebar__d-title--show');
			}
		});

		[].forEach.call(items, el => {
			let label = el.querySelector('.d-sidebar__item-label');

			label.addEventListener('click', event => {

				label.classList.toggle('added')
				if (label.classList.contains('added')) {
					arrTitle.push(label.textContent);
				} else {
					arrTitle.pop(label.textContent);
				}

				txtTitle = arrTitle.join(', ');
				dTitle.style.color = '#333333';
				dTitle.textContent = txtTitle;

				if (txtTitle.length > 20) {
					dTitle.classList.add('d-sidebar__d-title--tomuch');
				} else if (arrTitle.length === 0) {
					dTitle.textContent = 'Выберите';
					dTitle.style.color = '';
					dTitle.classList.remove('d-sidebar__d-title--tomuch');
				} else {
					dTitle.classList.remove('d-sidebar__d-title--tomuch');
				}
			});
		})
	});

	//
	return false;
}


const mobGood = () => {
	let checkEl = document.querySelectorAll('.good');
	if (checkEl.length === 0) return false;

	let good = document.querySelector('.good'),
		title = good.querySelector('.good__title'),
		bread = good.querySelector('.good__bread'),
		content = good.querySelector('.good__content');

	let check = () => {
		let winWidth = document.documentElement.clientWidth || document.body.clientWidth;

		if (winWidth < 768) {
			good.insertBefore(title, good.firstChild);
			good.insertBefore(bread, good.firstChild);
		} else if (winWidth >= 768) {
			content.insertBefore(title, content.firstChild);
			content.insertBefore(bread, content.firstChild);
		}
	}

	check();
	window.addEventListener('resize', check);

	// --
	return false;
}

const mobGProduction = () => {
	let checkEl = document.querySelectorAll('.g-production');
	if (checkEl.length === 0) return false;

	let production = document.querySelector('.g-production'),
		wrapSerts = production.querySelector('.g-production__wrap-serts'),
		inner = production.querySelector('.g-production__inner'),
		content = production.querySelector('.g-production__content');

	let check = () => {
		let winWidth = document.documentElement.clientWidth || document.body.clientWidth;

		if (winWidth < 768) {
			inner.appendChild(wrapSerts);
		} else if (winWidth >= 768) {
			content.appendChild(wrapSerts);
		}
	}

	check();
	window.addEventListener('resize', check);
	// --
	return false;
}

const mobCArticle = () => {
	let checkEl = document.querySelectorAll('.c-article--good');
	if (checkEl.length === 0) return false;

	let article = document.querySelector('.c-article--good'),
		title = article.querySelector('.c-article__title'),
		content = article.querySelector('.c-article__content');

	let check = () => {
		let winWidth = document.documentElement.clientWidth || document.body.clientWidth;

		if (winWidth < 768) {
			article.insertBefore(title, article.firstChild);
		} else if (winWidth >= 768) {
			content.insertBefore(title, content.firstChild);
		}
	}

	check();
	window.addEventListener('resize', check);
	// --
	return false;
}

const mobICalc = () => {
	let checkEl = document.querySelectorAll('.i-calc');
	if (checkEl.length === 0) return false;

	let iCalc = document.querySelector('.i-calc'),
		btn = iCalc.querySelector('.i-calc__order'),
		output = iCalc.querySelector('.i-calc__output'),
		input = iCalc.querySelector('.i-calc__input');

	let check = () => {
		let winWidth = document.documentElement.clientWidth || document.body.clientWidth;

		if (winWidth < 1280) {
			output.appendChild(btn);
		} else if (winWidth >= 1280) {
			input.appendChild(btn);
		}
	}

	check();
	window.addEventListener('resize', check);
	// --
	return false;
}