$(window).on('load', function () {

    if ( $('#map').length > 0 ) {

        ymaps.ready(initYaMap);
    }
});


function initYaMap() {

	var myMap = new ymaps.Map('map', {
            center: [59.92818216, 30.35662057],
            zoom: 16,
            controls: []
        }, {
            searchControlProvider: 'yandex#search'
        }),

        myPlacemark = new ymaps.Placemark([59.92818216, 30.35662057], {
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'assets/images/static/map-marker.svg',
            iconImageSize: [100, 70],
            iconImageOffset: [-40, -50]
        });

    myMap.geoObjects
        .add(myPlacemark);

    myMap.behaviors.disable('scrollZoom');
    
    let winWidth = document.documentElement.clientWidth || document.body.clientWidth;
    if (winWidth > 1024) return false;
	ymapsTouchScroll(myMap);

}
