<?

class LOG_INTERFACE {
    /** Log message with label
     *  для вывода в лог файл по дням года с иерархией папок
     * @param mixed $message
     * @param string $label
     * @param string $path
     */
    static function LogMessage ($message, $label = '', $path = '') {
        $backTrace = debug_backtrace ();
        $logPathPart = array (
            'logs',
            date ('Y'),
            date ('m'),
            date ('d')
        );
        $logPath = $_SERVER['DOCUMENT_ROOT'].'/';

        foreach ($logPathPart as $part) {
            $logPath .= $part.'/';

            if (!file_exists ($logPath)) {
                mkdir ($logPath, 0700);
            }

            if ($part == 'logs') {
                $htAccess = fopen ($logPath.'.htaccess', 'w');
                fwrite ($htAccess, 'Deny from all');
                fclose ($htAccess);
            }
        }

        $path = $logPath.(mb_strlen ($path) == 0 ? 'log.txt' : $path);
        $file = fopen ($path, 'a+');
        fwrite ($file, date ('r', time ())."\t".$label.': '.print_r ($message, true)." (line: ".$backTrace[0]['line'].", file: ".$backTrace[0]['file'].")\n");
        fclose ($file);
    }



    /**
     * Вывод переменной в консоль
     *
     * @param mixed $var
     * @param string $label
     * @param bool $useBackTrace
     * @param bool $toString
     *
     * @return string
     */
    static function debugToConsole ($var, $label = '', $useBackTrace = true, $toString = false) {
        $backTrace = array ();
        if ($useBackTrace) {
            $backTrace = debug_backtrace ();
        }

        $json = json_encode (unserialize(str_replace(array('NAN;','INF;'),'0;',serialize($var))));
        if ($json)
            $js = '<script type="text/javascript"> 
console.log('.(empty($label) ? '' : '\''.$label.'\',').$json.');//
'.($useBackTrace ? 'console.log("line: '.$backTrace[0]['line'].', file: '.$backTrace[0]['file'].'")' : '').'; 
</script>';
        else {
            $js = '<script type="text/javascript"> 
console.log('.(empty($label) ? '' : '\''.$label.'\',').json_last_error_msg().');//
'.($useBackTrace ? 'console.log("line: '.$backTrace[0]['line'].', file: '.$backTrace[0]['file'].'")' : '').'; 
</script>';
        }

        if (!$toString) {
            echo $js;

            return '';
        } else {
            return $js;
        }
    }
}
