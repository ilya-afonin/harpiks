<?
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/php_interface/include/classes/FirePHP.class.php');
$firephp = FirePHP::getInstance(true);
$options = array(
    'maxObjectDepth' => 15,
    'maxArrayDepth' => 15,
    'maxDepth' => 15,
    'useNativeJsonEncode' => true,
    'includeLineNumbers' => true
);
$firephp->setOptions($options);
include_once($_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/classes/log.php");

if(!defined('CUR_DIR')){
    define('CUR_DIR',$APPLICATION->GetCurDir());
}

define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");

define('CATALOG_IBLOCK_ID',2);
define('SOLUTIONS_IBLOCK_ID',4);
define('ARTICLES_IBLOCK_ID',5);

/***EVENTS***/
AddEventHandler("iblock", "OnAfterIBlockElementAdd", "OnAfterIBlockElementUpdateHandler");
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", "OnAfterIBlockElementUpdateHandler");

function OnAfterIBlockElementUpdateHandler(&$arFields) {
    if($arFields['IBLOCK_ID']==SERVICES_IBLOCK_ID){
        global $CACHE_MANAGER;
        $CACHE_MANAGER->ClearByTag("iblock_id_".SERVICE_TYPE_IBLOCK_ID);
    }
}



        /**
 * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
 * param  $number Integer Число на основе которого нужно сформировать окончание
 * param  $endingsArray  Array Массив слов или окончаний для чисел (1, 4, 5),
 *         например array('яблоко', 'яблока', 'яблок')
 * return String
 */
function getNumEnding($number, $endingArray) {
    $number = $number % 100;
    if ($number >= 11 && $number <= 19) {
        $ending = $endingArray[2];
    } else {
        $i = $number % 10;
        switch ($i) {
            case (1):
                $ending = $endingArray[0];
                break;
            case (2):
            case (3):
            case (4):
                $ending = $endingArray[1];
                break;
            default:
                $ending = $endingArray[2];
        }
    }
    return $ending;
}



/*AddEventHandler("main", "OnEndBufferContent", "ChangeMyContent");
function ChangeMyContent($content)
{
    FirePHP::getInstance()->info('sds');
    global $APPLICATION;
//    AddMessage2Log('ChangeMyContent');
//    $url = $APPLICATION->GetCurPage();
//    if(!substr_count($url, "tpl") && !substr_count($url, "bitrix") ){
        //  $content = str_replace("","", $content);
    $content = preg_replace("/(.*)<html/", "<html", $content);
//    }
}
*/
