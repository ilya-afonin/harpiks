<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("Страница не найдена");?>

    <div class="c-top">
      <div class="c-top__inner">
        <div class="c-top__aside"></div>
        <div class="c-top__cont">
          <div class="not-found">
            <div class="not-found__t-wrap">
              <h1 class="not-found__title">Ничего не найдено!</h1>
              <div class="not-found__desc">К сожалению, такой страницы не существует.</div>
              <a class="link link--blue link--bordered not-found__back" href="/">Вернуться на главную</a>
            </div>
            <div class="not-found__n-wrap">
              <div class="not-found__num">404</div>
              <div class="not-found__bg"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>